kind: "Template"
apiVersion: v1
metadata:
  name: "openshift-gitlab-runner-deploymentconifg"
  annotations:
    openshift.io/display-name: "OpenShift GitLab Runner"
    description: "Deploys a GitLab Runner container which runs in OpenShift with a non-privileged user."
    tags: "gitlab,runner"
    iconClass: "icon-git"
    template.openshift.io/long-description: "This template defines resources needed to deploy a GitLab Runner so that it can be used in CI/CD pipelines that make calls to the OpenShift API."
    template.openshift.io/provider-display-name: "OpenShift."
message: "The GitLab Runner has been deployed."
labels:
  template: "openshift-gitlab-runner-deploymentconfig"
objects:
- apiVersion: v1
  kind: ImageStream
  metadata:
    name: openshift-gitlab-runner
  spec:
    lookupPolicy:
      local: false
    tags:
    - annotations: null
      from:
        kind: DockerImage
        name: registry.gitlab.com/cmeidlin/openshift-gitlab-runner:latest
      importPolicy:
        scheduled: true
      name: latest
      referencePolicy:
        type: Source
- apiVersion: v1
  kind: Secret
  metadata:
    name: openshift-gitlab-runner-secrets
  stringData:
    GITLAB_TOKEN: ${GITLAB_TOKEN}
    GITLAB_URL: ${GITLAB_URL}
    RUNNER_TAG_LIST: ${RUNNER_TAG_LIST}
    OC_TOKEN: ${OC_TOKEN}
    OPENSHIFT_BUILD_NAMESPACE: ${OPENSHIFT_BUILD_NAMESPACE}
    K8S_PULL_POLICY: ${K8S_PULL_POLICY}
    K8S_IMAGE_PULL_SECRETS: ${K8S_IMAGE_PULL_SECRETS}
    RUNNER_OPTIONS: ${RUNNER_OPTIONS}
- kind: "DeploymentConfig"
  apiVersion: "v1"
  metadata:
    name: "openshift-gitlab-runner"
    annotations:
      description: "Defines how to deploy the GitLab Runner."
    labels:
      app: "openshift-gitlab-runner"
  spec:
    strategy:
      type: "Rolling"
    triggers:
      - type: "ImageChange"
        imageChangeParams:
          automatic: true
          containerNames:
            - "openshift-gitlab-runner"
          from:
            kind: "ImageStreamTag"
            name: "openshift-gitlab-runner:latest"
      - type: "ConfigChange"
    replicas: 1
    selector:
      name: "openshift-gitlab-runner"
    template:
      metadata:
        name: "openshift-gitlab-runner"
        labels:
          name: "openshift-gitlab-runner"
          app: "openshift-gitlab-runner"
      spec:
        containers:
          - env:
            - name: GITLAB_URL
              valueFrom:
                secretKeyRef:
                  key: GITLAB_URL
                  name: openshift-gitlab-runner-secrets
            - name: GITLAB_TOKEN
              valueFrom:
                secretKeyRef:
                  key: GITLAB_TOKEN
                  name: openshift-gitlab-runner-secrets
            - name: RUNNER_TAG_LIST
              valueFrom:
                secretKeyRef:
                  key: RUNNER_TAG_LIST
                  name: openshift-gitlab-runner-secrets
            - name: OC_TOKEN
              valueFrom:
                secretKeyRef:
                  key: OC_TOKEN
                  name: openshift-gitlab-runner-secrets
            - name: OPENSHIFT_BUILD_NAMESPACE
              valueFrom:
                secretKeyRef:
                  key: OPENSHIFT_BUILD_NAMESPACE
                  name: openshift-gitlab-runner-secrets
            - name: K8S_PULL_POLICY
              valueFrom:
                secretKeyRef:
                  key: K8S_PULL_POLICY
                  name: openshift-gitlab-runner-secrets
            - name: K8S_IMAGE_PULL_SECRETS
              valueFrom:
                secretKeyRef:
                  key: K8S_IMAGE_PULL_SECRETS
                  name: openshift-gitlab-runner-secrets
            - name: RUNNER_OPTIONS
              valueFrom:
                secretKeyRef:
                  key: RUNNER_OPTIONS
                  name: openshift-gitlab-runner-secrets
            name: "openshift-gitlab-runner"
            image: openshift-gitlab-runner
parameters:
- description: "The URL of the GitLab Server you will be configuring your GitLab Runner to."
  displayName: "GitLab URL"
  name: "GITLAB_URL"
  required: true
  value: "https://gitlab.com"
- description: "The token you got from GitLab for this Runner."
  displayName: "GitLab Token"
  name: "GITLAB_TOKEN"
  required: true
  value: ""
- description: "The token you generated for your OpenShift project service account."
  displayName: "OpenShift Service Account Token"
  name: "OC_TOKEN"
  required: false
  value: ""
- description: "Project/namespace to be used for build."
  displayName: "OpenShift Built Namespace"
  name: "OPENSHIFT_BUILD_NAMESPACE"
  required: false
  value: ""
- description: "list of runner tags, comma separated"
  displayName: "Gitlab Runner Tags"
  name: "RUNNER_TAG_LIST"
  required: false
  value: ""
- description: "Project/namespace that contains the ImageStream for the openshift-gitlab-runner container image"
  displayName: "Gitlab Runner Image Location (project/namespace)"
  name: "RUNNER_IMAGE_NAMESPACE"
  required: false
  value: "openshift"
- description: "Pull policy for build images.  Defaults to 'always'"
  displayName: "K8S Pull Policy"
  name: "K8S_PULL_POLICY"
  required: false
  value: "always"
- description: "Secret to be used when pulling container images within the pipeline"
  displayName: "K8S Image Pull Secrets"
  name: "K8S_IMAGE_PULL_SECRETS"
  required: false
  value: "internal-registry"
- description: "Additional options to pass to gitlab-runner register command"
  displayName: "Gitlab-runner register options"
  name: "RUNNER_OPTIONS"
  required: false
  value: ""
