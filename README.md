# openshift-gitlab-runner

* [Description](#description)
* [Architecture](#architecture)
* [Setup](#setup)
* [Operation](#operation)

## Description

OpenShift compatible [runner](https://docs.gitlab.com/runner/) for [GitLab](https://gitlab.com/).

## Architecture

![gitlab-runner](img/openshift-gitlab-runner.png)

This example is separated into multiple projects to allow for more granular access control and assignment of responsibilities.  It is possible to build, deploy, and operate the runner in a single Project if you like.

The Projects are:

1. **gitlab-serviceaccounts** (OCP_SA_PROJECT): A project for storing service account that the runner can use to create OpenShift Projects, deploy applications, perform tests, and any other operations that your pipeline executes.  In this example, we use a single service account called `gitlab-ci-default`. 
2. **gitlab-runner-image** (RUNNER_BUILD_PROJECT): A project for building the gitlab-runner container image.  The build process pulls a Dockerfile from the gitlab repository, builds the gitlab-runner container image, and then pushes it to a gitlab container registry.  The default image name is `openshift-gitlab-runner:test`.
  
3. **myapp-runner** (APP_RUNNER_PROJECT): A project for deploying the runner.  There will be a separate project for each OpenShift application deployed.  The container image is pulled from the gitlab container registry (default image name is `openshift-gitlab-runner:latest`) and launched with the options provided during deployment.
4. **myapp-build** (APP_BUILD_PROJECT): A project for building your application.  There will be a separate project for each OpenShift application deployed. Although any image specified by your pipeline can by used here, in order for your pipeline to create OpenShift projects and otherwise interact with the cluster, you will have to specify a build image that contains the `oc` command line tool.  See the [ocp-tools Dockerfile](https://gitlab.com/cmeidlin/openshift-gitlab-runner/blob/master/ocp-tools/Dockerfile) for an example.
5. **feature-branch**: An ephemeral project created by your pipeline to deploy your application for testing, security scanning, etc.



## Setup

In the example setup shown below, the service account, runner, and build namespace have been assigned separate projects.  By using separate projects, organizations can more granularly define authorization over the gitlab runner components.  You may, instead, use a single project for all components.

### Obtaining required GitLab parameters

In order to use the GitLab Runner you will need to at least obtain two parameters from your GitLab repository. Both are located in the same page. First you need to go to your project home and from there click on the sidebar on `Settings` and then after that click on the settings sub-item `CI / CD`.

Expand the Runners section and look for the `Set up a specific Runner manually` instructions, from where you just need to take note of the URL and the token.

You will also need to provide credentials for registry.gitlab.com to allow OpenShift to pull down the runner code and push up the built container image.  Those credentials will map to the following variables in this document:

  * GIT_REPO_USER (git code)
  * GIT_REPO_PASS (git code)
  * CONTAINER_REPO_USER (container registry)
  * CONTAINER_REPO_PASS (container registry)


### Creating an OpenShift service account for use by the gitlab runner

In order to deploy new containers in OpenShift from the GitLab Runner you will need to obtain a token that the Runner will use to authenticate itself. It's recommended to set up a `service account` in OpenShift for this purpose and only give it the desired permissions. The easiest way of creating such an account is via the `oc` client command line tool. If you are logged in to OpenShift and you are admin of your project then you can create the service account by performing the following steps:

1. Determine the name of the project in which the service account will be created.  Determine the name of the service account.  In this example, we use the following parameters:

```
OCP_SA_PROJECT=gitlab-serviceaccounts
OCP_SA_NAME=gitlab-ci-default
```

2.  Create a project to store your service account(s).
```shell
oc new-project ${OCP_SA_PROJECT}
```


3.  Create the service account.
```shell
oc create serviceaccount ${OCP_SA_NAME} -n ${OCP_SA_PROJECT}
```

4.  Then you can obtain the token for your service account.
```shell
OCP_SA_TOKEN=`oc serviceaccounts get-token ${OCP_SA_NAME} -n ${OCP_SA_PROJECT}`
```

### Build the gitlab runner container image

**Note:**  To make the runner container image available to all of your projects, you can perform these steps in the `openshift` project.  Using a custom project like `gitlab-runner-image` allows those without write access to `openshift` the ability to download, build, and test the runner.

In this example, the following OpenShift project is used for building the gitlab-runner conainer image:

  ```
  RUNNER_BUILD_PROJECT=gitlab-runner-image
  ```

Complete list of variables used in this section:

  ```
  RUNNER_BUILD_PROJECT=
  RUNNER_VERSION=
  CONTAINER_REPO_USER=
  CONTAINER_REPO_PASS=
  GIT_REPO_USER=
  GIT_REPO_PASS=
  ```

1. Make sure your `oc` client is logged in to the desired OpenShift cluster.

2. Create a new project to build the runner and store the ImageStream

    ```shell
    oc new-project ${RUNNER_BUILD_PROJECT}
    ```

3. Download the `openshift-gitlab-runner-bc-template.yaml` file and run the following to process and then create the desired resources:

    Note:  In this example, gitlab-runner-12.3.0 will be specified.

    ```
    RUNNER_VERSION=-12.3.0
    ```

    ```shell
    git clone https://gitlab.com/cmeidlin/openshift-gitlab-runner.git
    cd openshift-gitlab-runner
    oc create secret docker-registry gitlab-image-push --docker-server=registry.gitlab.com --docker-username=${CONTAINER_REPO_USER} --docker-password=${CONTAINER_REPO_PASS} -n ${RUNNER_BUILD_PROJECT}
    oc process -f openshift/openshift-gitlab-runner-bc-template.yaml -p RUNNER_VERSION=${RUNNER_VERSION} GIT_REPO_USER=${GIT_REPO_USER} GIT_REPO_PASS=${GIT_REPO_PASS} | oc create -f -
    ```

### Deploy an instance of the gitlab runner

Now that the runner container image has been built and resides in your cluster registry, one or more instances of the runner can be deployed.  In the example steps below, one instance of the runner will be deployed to a project named `myapp-runner`.  That instance of the runner will use a project called `myapp-build`

  ```
  APP_RUNNER_PROJECT=myapp-runner
  APP_BUILD_PROJECT=myapp-build
  ```
Complete list of variables used in this section:

  ```
  APP_RUNNER_PROJECT=
  APP_BUILD_PROJECT=
  OCP_SA_PROJECT=
  OCP_SA_NAME=
  OCP_SA_TOKEN=
  CONTAINER_REPO_USER=
  CONTAINER_REPO_PASS=
  K8S_IMAGE_PULL_SECRETS=
  GITLAB_URL=
  GITLAB_TOKEN=
  RUNNER_TAG_LIST=
  K8S_PULL_POLICY=
  RUNNER_OPTIONS=
  ```

1. Make sure your `oc` client is logged in to the desired OpenShift cluster.

2. (Optionally) Create a new project for your builds.

    ```shell
    oc new-project ${APP_BUILD_PROJECT}
    ```

3. Grant access to the build project to your OpenShift service account.

    ```shell
    oc policy add-role-to-user edit system:serviceaccount:${OCP_SA_PROJECT}:${OCP_SA_NAME} -n ${APP_BUILD_PROJECT}
    ```

4. Create a new project for your instance of the runner

    ```shell
    oc new-project ${APP_RUNNER_PROJECT}
    ```

4. Grant the service accounts in this project access to the upstream image in the gitlab registry.  

    ```shell
    oc create secret docker-registry gitlab --docker-server=gitlab.com --docker-username=${CONTAINER_REPO_USER} --docker-password=${CONTAINER_REPO_PASS} -n ${APP_RUNNER_PROJECT}
    oc create secret docker-registry gitlab-registry --docker-server=registry.gitlab.com --docker-username=${CONTAINER_REPO_USER} --docker-password=${CONTAINER_REPO_PASS} -n ${APP_RUNNER_PROJECT}
    oc secrets link default gitlab --for=pull -n ${APP_RUNNER_PROJECT}
    oc secrets link default gitlab-registry --for=pull -n ${APP_RUNNER_PROJECT}
    ```
5. If your service account needs to pull images from the integrated registry, you will need to create a `docker-registry` type secret containing the service accounts and make it available to your application's instance of the runner:

    ```
    K8S_IMAGE_PULL_SECRETS=internal-registry
    ```

    ```
    oc create secret docker-registry ${K8S_IMAGE_PULL_SECRETS} --docker-username=${OCP_SA_NAME} --docker-password=${OCP_SA_TOKEN} --docker-server=image-registry.openshift-image-registry.svc:5000 -n ${APP_BUILD_PROJECT}
    ```


5. Download the `openshift-gitlab-runner-dc-template.yaml` file and run the following to process and then create the desired resources:

    ```shell
    git clone https://gitlab.com/cmeidlin/openshift-gitlab-runner.git
    oc process -f openshift/openshift-gitlab-runner-dc-template.yaml \
      -p GITLAB_URL=https://gitlab.com/ \
         GITLAB_TOKEN=${GITLAB_TOKEN} \
         OC_TOKEN=${OCP_SA_TOKEN} \
         OPENSHIFT_BUILD_NAMESPACE=${APP_BUILD_PROJECT} \
         RUNNER_TAG_LIST=test-1,test-2 \
         K8S_PULL_POLICY=always \
         K8S_IMAGE_PULL_SECRETS=${K8S_IMAGE_PULL_SECRETS} \
         RUNNER_OPTIONS="${RUNNER_OPTIONS}" \
         | oc create -f - -n ${APP_RUNNER_PROJECT}
    ```

Notes:

  * GITLAB_URL:  The URL in this example is for the public gitlab.com server.  If you are running a private server, this URL will be different.  See the Obtaining required GitLab parameters section above.
  * GITLAB_TOKEN:  Token for your code repository.  See the Obtaining required GitLab parameters section above.
  * RUNNER_TAG_LIST:  Comma separated list of tags that you can associate with this instance of the runner.
  * K8S_PULL_POLICY:  Policy for pulling images (always, never, etc)
  * RUNNER_OPTIONS:  Any addtional options that you want to pass to the `gitlab-runner register` command.  For example:

    ```
    RUNNER_OPTIONS="--kubernetes-memory-request 2G --kubernetes-memory-limit 2G"
    ```

### Verifying Runner Registration

Go to your repository in the GitLab Web Interface and then on the left sidebar click on `Settings -> CI / CD` you should be able to see your runner under the `Runners activated for this project` section. If the Runner has a green circle next to its name it means that the Runner is ready to be used.

## Operation

### Example gitlab-ci.yml file

```
image: image-registry.openshift-image-registry.svc:5000/ocp-tools-builder/ocp-tools

test:
  tags:
    - test-1
  script:
    - oc login --token=${OC_TOKEN} https://console.openshift.example.com:6443
    - oc new-project $myproject
    - etc...
```

### Updating tags on a runner instance

#### Update the secret

1. In the OpenShift web console, navigate to **Workloads > Secrets** in the project which contains your runner instance (`gitlab-runner-instance1` in the example above).
2. Click the `openshift-gitlab-runner-secrets` secret, then click **Actions > Edit Secret**
3. Edit the tag list and click **Save**

#### Restart the pod

1. Navigate to **Workloads > Pods** in the project which contains your runner instance (`gitlab-runner-instance1` in the example above).
2. Click the "three dots" menu at the right of the runner pod and select **Delete Pod**
3. Verify the tag update on your gitlab code repository.

### Registering Runners with multiple repositories

Currently, it is not possible to register the runner from this project with multiple repositories as part of a standard deployment.  A runner can be registered with multiple projects *temporarily* but those registrations are ephemeral and will not persist across pod deletions or moves from one OpenShift node to another.  This might be useful for testing purposes.

To register a runner with an addtional repository, `oc rsh` into the running pod and run the `gitlab-runner register` command by running the `/entrypoint` script.

  `/entrypoint register --env "HOME"="/tmp" --env "OC_TOKEN"="$OC_TOKEN"`

This will allow you to interactively provide the following values and register the runner with an additional repository.

* Gitlab-ci coordinator URL (e.g. https://gitlab.com/)
* The gitlab-ci token for this runner
* The gitlab-ci description for this runner
* The gitlab-ci tags for this runner (comma separated)
* The executor: (select "kubernetes")
