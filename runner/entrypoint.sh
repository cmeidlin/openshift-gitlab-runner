# Ensure that assigned uid has entry in /etc/passwd
if [ `id -u` -ge 10000 ]; then
    cat /etc/passwd | sed -e "s/^gitlab-runner:/builder:/" > /tmp/passwd
    echo "gitlab-runner:x:`id -u`:`id -g`:,,,:/home/gitlab-runner:/bin/bash" >> /tmp/passwd
    cat /tmp/passwd > /etc/passwd
    rm /tmp/passwd
fi

unregister() {
    echo "Stopping and unregistering the GitLab runner."
    gitlab-runner stop
    gitlab-runner unregister --all-runners
}

trap unregister SIGTERM

gitlab-runner register --non-interactive \
 --description "OpenShift GitLab Runner" \
 --url $GITLAB_URL --registration-token $GITLAB_TOKEN \
 --tag-list $RUNNER_TAG_LIST \
 --builds-dir "/tmp" \
 --env "HOME=/tmp" \
 --env "OC_TOKEN=$OC_TOKEN" \
 --executor "kubernetes" \
 --kubernetes-namespace $OPENSHIFT_BUILD_NAMESPACE \
 --kubernetes-cpu-limit "1" \
 --kubernetes-memory-limit "1Gi" \
 --kubernetes-service-cpu-limit "1" \
 --kubernetes-service-memory-limit "1Gi" \
 --kubernetes-bearer_token $OC_TOKEN \
 --kubernetes-pull-policy $K8S_PULL_POLICY \
 --kubernetes-image-pull-secrets $K8S_IMAGE_PULL_SECRETS $RUNNER_OPTIONS

gitlab-runner run &

child=$!
wait "$child"
